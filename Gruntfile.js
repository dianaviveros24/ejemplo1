// cuando ejecute una tarea va a poder checear dependencias para ejecultarlas cuando sea necesario
module.exports = function (grunt) {
    grunt.initConfig({
        sass: {
            files: [{
                expand: true,
                cwd: 'css',
                src: ['*.scss'],
                dest: 'css',
                ext: '.css'
            }]
        },

        watch: {
            files: ['css/*.scss'],
            task: ['css']
        }
    });
    // Agreagar tareas
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.registerTask('css', ['sass']);

};
