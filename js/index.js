$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 2000
    });
    $('#exampleModal').on('show.bs.modal', function (e) {
        console.log('el modal se esta mostrando');
        $('#contactobtn').removeClass('btn-primary');
        $('#contactobtn').addClass('btn-outline-primary');
    });
    $('#exampleModal').on('shown.bs.modal', function (e) {
        console.log('el modal se mostró');
    });
    $('#exampleModal').on('hide.bs.modal', function (e) {
        console.log('el modal se oculta');
    });
    $('#exampleModal').on('hidden.bs.modal', function (e) {
        console.log('el modal se ocultó');
    });
});